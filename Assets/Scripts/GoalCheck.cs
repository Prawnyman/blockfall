﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class GoalCheck : MonoBehaviour {

	// Use this for initialization


	public bool goalScored;

    //Brighter color
    public Color originalColor;
    //Duller color
    public Color endColor;
    //Should be bright too
    public Color goal_acheived_color;

    public Material goal_mat;
    public float duration;

    Tween goal_color_tween;

    private void Start()
    {
        goal_color_tween = goal_mat.DOColor(endColor, duration).SetLoops(-1, LoopType.Yoyo);
    }

    string wood = "WoodBlock";
    string endzone = "EndZone";
    void OnTriggerEnter(Collider col) {
		if (col.CompareTag(wood)) {
            SetShaderColor(goal_acheived_color);
			goalScored = true;
            col.GetComponentInChildren<Animator>().SetTrigger(endzone);
            goal_color_tween.Pause();
		}
	}

	void OnTriggerStay(Collider col) {
        if (col.CompareTag(wood))
        {
            SetShaderColor(goal_acheived_color);
			goalScored = true;
            goal_color_tween.Pause();
        }
	}

	void OnTriggerExit(Collider col) {
        if (col.CompareTag(wood))
        {
            SetShaderColor(originalColor);
			goalScored = false;
            goal_color_tween.Play();
        }
	}

    void SetShaderColor(Color color)
    {
        goal_mat.color = color;
    }

}
