﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.BasicApi;

public class SignInGoogle : MonoBehaviour {
    private GameObject obj;

    public delegate void GooglePlay();
    public static event GooglePlay OnFinishSignIn;

    void BuildGooglePlayConfig()
    {
        
        // recommended for debugging:
        PlayGamesPlatform.DebugLogEnabled = true;
        // Activate the Google Play Games platform
        PlayGamesPlatform.Activate();
        ((PlayGamesPlatform)Social.Active).SetGravityForPopups(Gravity.BOTTOM);
    }

    // Use this for initialization
    void Awake()
    {
        if (!PlayGamesPlatform.Instance.IsAuthenticated())
        {
            BuildGooglePlayConfig();
            Social.localUser.Authenticate((bool success) =>
            {
                if (success)
                {
                    Debug.Log("Succesful google play authentication");
                    OnFinishSignIn();
                };
            });
        }
    }


    public void ShowAcheivements()
    {
        Social.ShowAchievementsUI();
    }
}
