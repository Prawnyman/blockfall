﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.BasicApi;
using UnityEngine.SceneManagement;

public class AchievementTracker : MonoBehaviour
{

    private void OnEnable()
    {
        SignInGoogle.OnFinishSignIn += SignInGoogle_OnFinishSignIn;
        SceneManager.sceneLoaded += LevelLoaded;
        GoalChecker.OnFinishedGame += OnPuzzleEnd;
    }

    private void LevelParsed(int world, int level)
    {
        throw new System.NotImplementedException();
    }

    private void OnDisable()
    {
        SignInGoogle.OnFinishSignIn -= SignInGoogle_OnFinishSignIn;
        SceneManager.sceneLoaded -= LevelLoaded;
        GoalChecker.OnFinishedGame -= OnPuzzleEnd;
    }

    private void LevelLoaded(Scene scene, LoadSceneMode mode)
    {
        if (!scene.name.Contains("Level"))
            return;

        //We handle completing the world here
        int bit = PlayerPrefs.GetInt(StaticString.WorldCompleteMask, 0000);
        for (int i = 1; i < FlagsHelper.StatusList().Count; i++)
        {
            //If the flag is set
            if (FlagsHelper.IsSet(bit, (int)FlagsHelper.StatusList()[i]))
            {
                UnlockAcheivement(GetIDFromCompleteWorld(i), 100, (bool success) => {
                });
            }
        }

        //Next handle unlocking of world
        bit = PlayerPrefs.GetInt(StaticString.WorldUnlockedMask, 0001);
        for (int i = 1; i < FlagsHelper.StatusList().Count; i++)
        {
            //If the flag is set
            if (FlagsHelper.IsSet(bit, (int)FlagsHelper.StatusList()[i]))
            {
                UnlockAcheivement(GetIDfromUnlockWorld(i), 100, (bool success) => {
                    if(success)
                        UnlockAcheivement(AchievementIDs.achievement_unlock_all_worlds, 1 ,null);
                });
            }
        }
    }



    private void SignInGoogle_OnFinishSignIn()
    {
        //This is a first time entering, so we give them the achievement
        if(PlayerPrefs.GetInt("First Install", 0) == 0)
        {
            PlayerPrefs.SetInt("First Install", 1);
            UnlockAcheivement(AchievementIDs.achievement_first_install, 100, null);
        }
    }

    
    void UnlockAcheivement(string id, int increment_by_steps, System.Action<bool> CallBack)
    {
        if (increment_by_steps == 100)
        {
            if (!PlayGamesPlatform.Instance.GetAchievement(id).IsUnlocked)
            {
                PlayGamesPlatform.Instance.ReportProgress(id, increment_by_steps, (bool success_2) =>
                {
                    CallBack(success_2);
                });
            }
        }
        else
        {
            if (!PlayGamesPlatform.Instance.GetAchievement(id).IsUnlocked)
            {
                PlayGamesPlatform.Instance.IncrementAchievement(id, increment_by_steps, CallBack);
            }
        }
    }

    string GetIDFromCompleteWorld(int world)
    {
        switch (world)
        {
            case 1:
                return AchievementIDs.achievement_basic_world_complete;
            case 2:
                return AchievementIDs.achievement_urban_world_complete;
            case 3:
                return AchievementIDs.achievement_vortex_world_complete;
            case 4:
                return AchievementIDs.achievement_portal_world_complete;
        }
        return null;
    }

    string GetIDfromUnlockWorld(int world)
    {
        switch (world)
        {
            case 1:
                return AchievementIDs.achievement_unlock_basic_world;
            case 2:
                return AchievementIDs.achievement_unlock_urban_world;
            case 3:
                return AchievementIDs.achievement_unlock_vortex_world;
            case 4:
                return AchievementIDs.achievement_unlock_portal_world;
        }
        return null;
    }

    public void OnPuzzleEnd()
    {
        int local_world = PlayerPrefs.GetInt(StaticString.CurrentWorld, 0);
        int local_level = PlayerPrefs.GetInt(StaticString.CurrentLevel, 0);

        //Here we check for other achievemnets like iron man
        CheckForBlockAchievements(ref local_world, ref local_level);
        CheckAllGold(local_world);

        AchievementCheckWorldAndLevel(local_world, local_level);
    }

    void AchievementCheckWorldAndLevel(int local_world, int local_level)
    {
        //Now we move on
        for (int i = 1; i < 13; i++)
        {
            int Pref = PlayerPrefs.GetInt("Level" + local_world + "-" + i, -1);
            if (Pref > 0)
            {
                //At least one level is not complete. Break the cycle.
                return;
            }
        }

        //We have checked through all the levels of a certain world.
        WorldComplete(ref local_world);
    }

    void WorldComplete(ref int world)
    {
        switch(world)
        {
            case 1:
                UnlockAcheivement(AchievementIDs.achievement_basic_world_complete, 100, (bool success) => {
                    if (success)
                    {
                        UnlockAcheivement(AchievementIDs.achievement_complete_the_game, 1, null);
                    }
                });
                break;
            case 2:
                UnlockAcheivement(AchievementIDs.achievement_urban_world_complete, 100, (bool success) => {
                    if (success)
                    {
                        UnlockAcheivement(AchievementIDs.achievement_complete_the_game, 1, null);
                    }
                });
                break;
            case 3:
                UnlockAcheivement(AchievementIDs.achievement_vortex_world_complete, 100, (bool success) => {
                    if (success)
                    {
                        UnlockAcheivement(AchievementIDs.achievement_complete_the_game, 1, null);
                    }
                });
                break;
            case 4:
                UnlockAcheivement(AchievementIDs.achievement_portal_world_complete, 100, (bool success) => {
                    if (success)
                    {
                        UnlockAcheivement(AchievementIDs.achievement_complete_the_game, 1, null);
                    }
                });
                break;
            default:
                break;
        }
    }

    void WorldAllGold(ref int world)
    {
        switch (world)
        {
            case 1:
                UnlockAcheivement(AchievementIDs.achievement_all_gold_for_basic_world, 100, (bool success) => {
                    if (success)
                    {
                        UnlockAcheivement(AchievementIDs.achievement_all_levels_all_gold, 1, null);
                    }
                });
                break;
            case 2:
                UnlockAcheivement(AchievementIDs.achievement_all_gold_for_urban_world, 100, (bool success) => {
                    if (success)
                    {
                        UnlockAcheivement(AchievementIDs.achievement_all_levels_all_gold, 1, null);
                    }
                });
                break;
            case 3:
                UnlockAcheivement(AchievementIDs.achievement_all_gold_for_vortex_world, 100, (bool success) => {
                    if (success)
                    {
                        UnlockAcheivement(AchievementIDs.achievement_all_levels_all_gold, 1, null);
                    }
                });
                break;
            case 4:
                UnlockAcheivement(AchievementIDs.achievement_all_gold_for_portal_world, 100, (bool success) => {
                    if (success)
                    {
                        UnlockAcheivement(AchievementIDs.achievement_all_levels_all_gold, 1, null);
                    }
                });
                break;
            default:
                break;
                
        }
    }

    public void CheckForBlockAchievements(ref int  world, ref int level)
    {
        if(world == 1 && level == 4)
        {
            UnlockAcheivement(AchievementIDs.achievement_iron_man, 100, null);
        }
        else if(world == 2 && level == 1)
        {
            UnlockAcheivement(AchievementIDs.achievement_one_waywhat, 100, null);
        }
        else if(world == 3 && level == 1)
        {
            UnlockAcheivement(AchievementIDs.achievement_vortex, 100, null);
        }
        else if(world == 4 && level == 1)
        {
            UnlockAcheivement(AchievementIDs.achievement_unlock_portal_world, 100, null);
        }
        else if(world == 4 && level == 12)
        {
            UnlockAcheivement(AchievementIDs.achievement_all_levels_unlocked, 100, null);
        }
    }

    public void CheckAllGold(int world)
    {
        for (int i = 1; i < 13; i++)
        {
            int Pref = PlayerPrefs.GetInt("Level" + world + "-" + i, -1);
            if (Pref < 4)
            {
                //At least one level is not complete. Break the cycle.
                return;
            }
        }

        
    }
}

