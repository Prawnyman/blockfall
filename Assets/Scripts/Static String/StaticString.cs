﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticString : MonoBehaviour {
    public static string MasterGameScene = "MasterGameScene";
    public static string CurrentWorld = "CurrentWorld";
    public static string CurrentLevel = "CurrentLevel";

    public static string Left = "Left";
    public static string Right = "Right";

    public static string GoldRank = "Gold Rank!";
    public static string SilverRank = "Silver Rank!";
    public static string BronzeRank = "Bronze Rank!";
    public static string RunnerUpRank = "Runner Up!";

    public static string WorldCompleteMask = "WorldComplete_Bitmask";
    public static string WorldUnlockedMask = "WorldUnlocked_Bitmask";
    //public static string FinishWorldOne = "Finish World One";
    //public static string FinishWorldTwo = "Finish World Two";
    //public static string FinishWorldThree = "Finish World Three";
    //public static string FinishWorldFour = "Finish World Four";



}
