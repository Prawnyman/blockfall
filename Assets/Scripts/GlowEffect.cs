﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GlowEffect : MonoBehaviour {
    Outline outline;
    public Color InitialColor;
    public Color FinalColor;
    public float duration;

	// Update is called once per frame
	void Start () {
        outline = GetComponent<Outline>();
        StartLoop();
	}
    void StartLoop()
    {
        outline.DOColor(FinalColor, duration).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutQuad);
    }

}
