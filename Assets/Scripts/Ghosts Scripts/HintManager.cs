﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using AdvancedInspector;

public class HintManager : MonoBehaviour {
    public RotateGrid grid;
    Sequence fingerSequence = null;

    [Inspect, Group("Tutorial Variables")]
    public GameObject UI_Block_Mask;

    [Inspect, Group("Tutorial Variables")]
    public Transform PathParent;

    [Inspect, Group("Tutorial Variables")]
    public Transform [] Path;

    [Inspect, Group("Tutorial Variables")]
    public void UpdatePath()
    {
        Path = new Transform[PathParent.childCount];
        for(int i = 0; i < PathParent.childCount; i++)
        {
            Path[i] = PathParent.GetChild(i);
        }
    }

    [Inspect, Group("Tutorial Variables")]
    public Transform Finger;

    public int Moves_To_Hint
    {
        set
        {
            moves = value;
        }
    }
    private int moves;
    
    public GhostManager ghostManager;

    private void OnEnable()
    {
        RotateGrid.OnFinishedFalling += OnFinishFalling;
        RotateGrid.OnFinishedRotating += RotateGrid_OnFinishedRotating;
    }



    private void OnDisable()
    {
        RotateGrid.OnFinishedFalling -= OnFinishFalling;
        RotateGrid.OnFinishedRotating -= RotateGrid_OnFinishedRotating;
    }

    private void Start()
    {
        UI_Block_Mask.SetActive(false);
        Finger.gameObject.SetActive(false);
        if (PlayerPrefs.GetInt("FirstTime", 1) == 1)
        {
            //We show the tutorial
            PlayerPrefs.SetInt("FirstTime", 0);

            InitTutorial();
            StartCoroutine(ShowTutorial());
        }
    }

    [Inspect]
    void InitTutorial()
    {
        UI_Block_Mask.SetActive(true);
        Finger.transform.position = Path[0].position;
        Finger.gameObject.SetActive(true);

        fingerSequence = DOTween.Sequence();
        float time_multiplier = Time.timeScale;
        fingerSequence.Append(Finger.DOMove(Path[1].position, 2f * time_multiplier))
            .AppendInterval(0.5f * time_multiplier)
            .Append(Finger.DOScale(0.7f, 0.5f * time_multiplier))
            .AppendInterval(1f)
            .Append(Finger.DOMove(Path[2].position, 1f * time_multiplier))
            .AppendInterval(1f)
            .Append(Finger.DOScale(1f, 0.5f * time_multiplier))
            .AppendInterval(1f)
            .Append(Finger.DOMove(Path[3].position, 1f * time_multiplier).OnComplete(() => Finger.transform.position = Path[4].position))
            //.AppendInterval(0.5f * time_multiplier)

            .Append(Finger.DOMove(Path[5].position, 2f * time_multiplier))
            .AppendInterval(0.5f * time_multiplier)
            .Append(Finger.DOScale(0.7f, 0.5f * time_multiplier))
            .AppendInterval(1f)
            .Append(Finger.DOMove(Path[6].position, 1f * time_multiplier))
            .AppendInterval(1f)
            .Append(Finger.DOScale(1f, 0.5f * time_multiplier))
            .AppendInterval(1f)
            .Append(Finger.DOMove(Path[7].position, 1f * time_multiplier).OnComplete(() => Finger.transform.position = Path[0].position))
            //.AppendInterval(0.5f * time_multiplier)

            .SetLoops(-1, LoopType.Restart);


    }

    IEnumerator ShowTutorial()
    {
        Debug.Log("Showing Tutorial");
        yield return null;
    }

    private void RotateGrid_OnFinishedRotating()
    {
        if (fingerSequence != null)
        {
            fingerSequence.Kill();
            UI_Block_Mask.GetComponent<CanvasGroup>().DOFade(0, 0.5f).OnComplete( () => UI_Block_Mask.SetActive(false));
        }
    }

    private void OnFinishFalling()
    {
        if(grid.TimesMoved == moves)
        {
            ghostManager.Start_Ghosting();
        }
    }
}
