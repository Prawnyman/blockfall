﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GhostManager : MonoBehaviour {

    List<GameObject> ListOfGhosts = new List<GameObject>();
    List<Material> Materials = new List<Material>();
    List<Color> Colors = new List<Color>();
    List<Tween> tweens = new List<Tween>();

    public float ghost_duration = 2f;

    public void OnDisable()
    {
        Debug.Log("Killed " + DOTween.KillAll() + " tweens");
    }

    public void AddGhost(GameObject obj)
    {
        ListOfGhosts.Add(obj);
    }

    public List<GameObject> GetGhosts()
    {
        return ListOfGhosts;
    }

    public void GetMaterials()
    {
        try
        {
            for (int i = 0; i < ListOfGhosts.Count; i++)
            {
                if (!Materials.Contains(ListOfGhosts[i].GetComponent<MeshRenderer>().sharedMaterial))
                {
                    Materials.Add(ListOfGhosts[i].GetComponent<MeshRenderer>().sharedMaterial);
                }
                if (!Materials.Contains(ListOfGhosts[i].transform.GetComponentInChildren<Renderer>().sharedMaterial))
                {
                    Materials.Add(ListOfGhosts[i].transform.GetComponentInChildren<Renderer>().sharedMaterial);
                }
            }
        }
        catch (System.Exception)
        {
            Debug.LogWarning("Material not found, expected if loading from editor.");
        }
    }
    

    public void Init()
    {
        GetMaterials();

        Hide_Ghosts();
    }

    private void Hide_Ghosts()
    {
        for (int i = 0; i < Materials.Count; i++)
        {
            Colors.Add(Materials[i].color);
            Materials[i].color = Color.clear;
        }
    }

    public void Start_Ghosting()
    {
        for(int i = 0; i < Materials.Count; i++)
        {
            tweens.Add(Start_Blinking(Materials[i] , i ));
        }
    }

    Tween Start_Blinking(Material ghost_material, int index)
    {
        ghost_material.color = Colors[index];
        ghost_material.DOFade(0, 0);
        Tween tween = ghost_material.DOFade(Colors[index].a, ghost_duration).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InBounce);
        return tween;
    }
}
