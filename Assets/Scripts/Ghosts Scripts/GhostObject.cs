﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using AdvancedInspector;

public class GhostObject : MonoBehaviour {
    public Color GhostColor;

    [Inspect, Method(MethodDisplay.Invoke)]
    private void Awake()
    {
        GetComponent<MeshRenderer>().sharedMaterial.color = GhostColor;
    }
}
