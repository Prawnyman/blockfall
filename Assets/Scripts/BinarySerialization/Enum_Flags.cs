﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Flags]
public enum Worlds
{
    None = 0,
    World_One = 1 << 0,
    World_Two = 1 << 1,
    World_Three = 1 << 2,
    World_Four = 1 << 3,
}

public static class FlagsHelper
{
    public static bool IsSet<T>(T flags, T flag) where T : struct
    {
        int flagsValue = (int)(object)flags;
        int flagValue = (int)(object)flag;

        return (flagsValue & flagValue) != 0;
    }

    public static void Set<T>(ref T flags, T flag) where T : struct
    {
        int flagsValue = (int)(object)flags;
        int flagValue = (int)(object)flag;

        flags = (T)(object)(flagsValue | flagValue);
    }

    public static void Unset<T>(ref T flags, T flag) where T : struct
    {
        int flagsValue = (int)(object)flags;
        int flagValue = (int)(object)flag;

        flags = (T)(object)(flagsValue & (~flagValue));
    }

    public static List<Worlds> StatusList()
    {
        return new List<Worlds>
    {
        Worlds.None,
        Worlds.World_One,
        Worlds.World_Two,
        Worlds.World_Three,
        Worlds.World_Four
    };
    }
}