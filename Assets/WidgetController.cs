﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class WidgetController : MonoBehaviour {

    public GameObject backgroundBg;
    public float INduration;
    public float OUTduration;

    public void CloseThisWidget()
    {
        this.transform.DOScale(0, OUTduration).OnComplete( ()=> { backgroundBg.SetActive(false); });
    }

    public void OpenWidget()
    {
        backgroundBg.SetActive(true);
        this.transform.DOScale(1, INduration).SetEase(Ease.OutBounce);
    }
}
